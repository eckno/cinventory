﻿using System.Web;
using System.Web.Optimization;

namespace CInventory
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            #region Template Desing

            bundles.Add(new ScriptBundle("~/template/js").Include(


                      "~/Scripts/codebase.core.min.js",
                      "~/Scripts/codebase.app.min.js",
                      "~/Scripts/plugins/jquery-validation/jquery.validate.min.js",
                      "~/Scripts/pages/op_auth_signin.min.js",
                      "~/Scripts/ plugins / chartjs / Chart.bundle.min.js",
                      "~/Scripts/pages/be_pages_dashboard.min.js",
                      "~/Scripts/"
                      ));

            bundles.Add(new StyleBundle("~/template/css").Include(
                      "~/Content/css/codebase.min.css"

                      ));

            #endregion
        }
    }
}
