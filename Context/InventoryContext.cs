﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CInventory.Models;

namespace CInventory.Context
{
    public class InventoryContext : DbContext
    {
        public DbSet<productModel> products { get; set; }

        public DbSet<tblCompanies> tblCompanies { get; set; }
        public DbSet<tblCategories> tblCategories { get; set; }
        public DbSet<tblCustomers> tblCustomers { get; set; }
        public DbSet<tblInvoices> tblInvoices { get; set; }
        public DbSet<tblOrders> tblOrders { get; set; }
        public DbSet<tblProducts> tblProducts { get; set; }
        public DbSet<tblProductType> productTypes { get; set; }
        public DbSet<tblReceipts> tblReceipts { get; set; }
        public DbSet<tblReceived> tblReceives { get; set; }
        public DbSet<tblRegistry> tblRegistries { get; set; }
        public DbSet<tblSold> tblSolds { get; set; }
        public DbSet<tblUsers> tblUsers { get; set; }
        public DbSet<tblVendor> tblVendors { get; set; }
        public DbSet<tblWaybill> tblWaybills { get; set; }
    }
}