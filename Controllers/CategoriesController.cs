﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CInventory.Context;
using CInventory.Models;
using Microsoft.AspNet.Identity;

namespace CInventory.Controllers
{
    [Authorize]
    public class CategoriesController : Controller
    {
        private InventoryContext db = new InventoryContext();
        private ApplicationDbContext dba = new ApplicationDbContext();

        // GET: tblCategories
        public ActionResult Index()
        {
            var isUser = User.Identity.GetUserId();
            // string query = "SELECT companyID FROM dbo.AspNetUsers WHERE Id = @id";
            var id = dba.Users.Where(ui => ui.Id == isUser).Select(c => c.companyID).SingleOrDefault();
            return View(db.tblCategories.Where(i => i.companyID == id).ToList());
        }

        // GET: tblCategories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblCategories tblCategories = db.tblCategories.Find(id);
            if (tblCategories == null)
            {
                return HttpNotFound();
            }
            return View(tblCategories);
        }

        // GET: tblCategories/Create
        public ActionResult Create()
        {
            return HttpNotFound();
        }

        // POST: tblCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(tblCategories tblCategories)
        {
            
            if (ModelState.IsValid)
            {
                var catName = Request["isCatName"];
                var userID = User.Identity.GetUserId();
                var isCompany = dba.Users.Where(a => a.Id == userID).Select(c => c.companyID).FirstOrDefault();
                tblCategories.categoryName = catName;
                tblCategories.companyID = isCompany;
                tblCategories.createdBy = User.Identity.GetUserName();
                tblCategories.createdOn = DateTime.Now;
                db.tblCategories.Add(tblCategories);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblCategories);
        }

        // GET: tblCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblCategories tblCategories = db.tblCategories.Find(id);
            if (tblCategories == null)
            {
                return HttpNotFound();
            }
            return View(tblCategories);
        }

        // POST: tblCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,categoryName,createdOn,createdBy,companyID")] tblCategories tblCategories)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblCategories).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblCategories);
        }

        // GET: tblCategories/Delete/5
        public ActionResult Delete(int? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //tblCategories tblCategories = db.tblCategories.Find(id);
            //if (tblCategories == null)
            //{
            //    return HttpNotFound();
            //}
            return HttpNotFound();
        }

        // POST: tblCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblCategories tblCategories = db.tblCategories.Find(id);
            db.tblCategories.Remove(tblCategories);
            db.SaveChanges();
            return View(tblCategories);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
