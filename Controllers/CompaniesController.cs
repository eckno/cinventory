﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CInventory.Context;
using CInventory.Models;


namespace CInventory.Controllers
{
    public class CompaniesController : Controller
    {
        private InventoryContext db = new InventoryContext();

        // GET: Companies
        public async Task<ActionResult> Index()
        {
            return View(await db.tblCompanies.ToListAsync());
        }

        // GET: Companies/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblCompanies tblCompanies = await db.tblCompanies.FindAsync(id);
            if (tblCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tblCompanies);
        }

        // GET: Companies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Companies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "companyName,companyAddress,companyContactNo,companySlogan,companyEmail,companyWebsite,companyAdminEmail,companyAdminAuth,subscription")] tblCompanies tblCompanies)
        {
            if (ModelState.IsValid)
            {
                var isName = Request["companyName"];
                var isDate1 = DateTime.Now;
                var isDate = isDate1.ToString();
                Random numbers = new Random();
                var ranNo = numbers.Next();
                var nameSub = isName.Substring(0, isName.IndexOf(" "));
                var dateSub = isDate.Substring(0, isDate.IndexOf(" "));
                var numSub = ranNo.ToString();
                var isNum = numSub.Substring(0, 6);
                var isID = nameSub + dateSub + isNum;
                tblCompanies.companyID = isID;
                tblCompanies.createdOn = isDate1;

                db.tblCompanies.Add(tblCompanies);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tblCompanies);
        }

        // GET: Companies/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblCompanies tblCompanies = await db.tblCompanies.FindAsync(id);
            if (tblCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tblCompanies);
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,companyName,companyAddress,companyContactNo,companySlogan,companyEmail,companyWebsite,companyAdminEmail,companyAdminAuth,companyID,createdOn,subscription")] tblCompanies tblCompanies)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblCompanies).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tblCompanies);
        }

        // GET: Companies/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblCompanies tblCompanies = await db.tblCompanies.FindAsync(id);
            if (tblCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tblCompanies);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            tblCompanies tblCompanies = await db.tblCompanies.FindAsync(id);
            db.tblCompanies.Remove(tblCompanies);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
