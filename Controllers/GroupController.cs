﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CInventory.Context;
using CInventory.Models;
using Microsoft.AspNet.Identity;

namespace CInventory.Controllers
{
    [Authorize]
    public class GroupController : Controller
    {
        private InventoryContext db = new InventoryContext();
        private ApplicationDbContext dba = new ApplicationDbContext();

        // GET: Group
        public ActionResult Index()
        {
            var isUser = User.Identity.GetUserId();
            var CID = dba.Users.Where(ui => ui.Id == isUser).Select(c => c.companyID).SingleOrDefault();
            return View(db.productTypes.ToList());
        }

        // GET: Group/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblProductType tblProductType = db.productTypes.Find(id);
            if (tblProductType == null)
            {
                return HttpNotFound();
            }
            return View(tblProductType);
        }

        // GET: Group/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Group/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(tblProductType model)
        {
            var isUser = User.Identity.GetUserId();
            var isGroup = Request["groupName"];
            var isDate = DateTime.Now;
            var companyID = dba.Users.Where(i => i.Id == isUser).Select(c => c.companyID).FirstOrDefault();
            var createdBy = dba.Users.Where(i => i.Id == isUser).Select(c => c.displayName).FirstOrDefault();
            if (ModelState.IsValid)
            {
                model.typeName = isGroup;
                model.companyID = companyID;
                model.createdOn = isDate;
                model.createdBy = createdBy;
                db.productTypes.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Group/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblProductType tblProductType = db.productTypes.Find(id);
            if (tblProductType == null)
            {
                return HttpNotFound();
            }
            return View(tblProductType);
        }

        // POST: Group/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,typeName,createdOn,createdBy,companyID")] tblProductType tblProductType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblProductType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblProductType);
        }

        // GET: Group/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblProductType tblProductType = db.productTypes.Find(id);
            if (tblProductType == null)
            {
                return HttpNotFound();
            }
            return View(tblProductType);
        }

        // POST: Group/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblProductType tblProductType = db.productTypes.Find(id);
            db.productTypes.Remove(tblProductType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
