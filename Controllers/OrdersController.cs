﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CInventory.Context;
using CInventory.Models;

namespace CInventory.Controllers
{
    public class OrdersController : Controller
    {
        private InventoryContext db = new InventoryContext();

        // GET: tblOrders
        public ActionResult Index()
        {
            return View(db.tblOrders.ToList());
        }

        // GET: tblOrders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblOrders tblOrders = db.tblOrders.Find(id);
            if (tblOrders == null)
            {
                return HttpNotFound();
            }
            return View(tblOrders);
        }

        // GET: tblOrders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tblOrders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,productID,Qty,unitPrice,totalPrice,customerID,orderNo,orderStatus,createdOn,companyID")] tblOrders tblOrders)
        {
            if (ModelState.IsValid)
            {
                db.tblOrders.Add(tblOrders);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblOrders);
        }

        // GET: tblOrders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblOrders tblOrders = db.tblOrders.Find(id);
            if (tblOrders == null)
            {
                return HttpNotFound();
            }
            return View(tblOrders);
        }

        // POST: tblOrders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,productID,Qty,unitPrice,totalPrice,customerID,orderNo,orderStatus,createdOn,companyID")] tblOrders tblOrders)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblOrders).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblOrders);
        }

        // GET: tblOrders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblOrders tblOrders = db.tblOrders.Find(id);
            if (tblOrders == null)
            {
                return HttpNotFound();
            }
            return View(tblOrders);
        }

        // POST: tblOrders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblOrders tblOrders = db.tblOrders.Find(id);
            db.tblOrders.Remove(tblOrders);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
