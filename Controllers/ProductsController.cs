﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CInventory.Context;
using CInventory.Models;
using Microsoft.AspNet.Identity;

namespace CInventory.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {

        private InventoryContext db = new InventoryContext();
        private ApplicationDbContext dba = new ApplicationDbContext();
        [Authorize]
        // GET: Products
        public ActionResult Index()
        {
            var isUserID = User.Identity.GetUserId();
            var CID = dba.Users.Where(i => i.Id == isUserID).Select(cid => cid.companyID).SingleOrDefault();
            return View(db.tblProducts.Where(cid => cid.companyID == CID).ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblProducts tblProducts = db.tblProducts.Find(id);
            if (tblProducts == null)
            {
                return HttpNotFound();
            }
            return View(tblProducts);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            //var isUserID = User.Identity.GetUserId();
            //var CID = dba.Users.Where(i => i.Id == isUserID).Select(cid => cid.companyID).SingleOrDefault();
            ////product group listing
            //var prodGroup = db.productTypes.Where(pt => pt.companyID == CID).Select(tn => tn.typeName).ToList();
            //SelectList groupList = new SelectList(prodGroup, "typeName");
            ViewBag.GroupList = new SelectList(GetProductTypes(), "typeName", "typeName");
            //category listing
            //var catList = db.tblCategories.Where(cat => cat.companyID == CID).Select(cn => cn.categoryName).ToList();
            //SelectList categoryList = new SelectList(catList, "categoryName");
            ViewBag.CategoryList = new SelectList(GetCategories(), "categoryName", "categoryName");

            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tblProducts tblProducts, HttpPostedFileBase iconFile)
        {
            if(iconFile != null && iconFile.ContentLength > 0)
            {
                //Image Upload
                var fileName_t = Path.GetFileNameWithoutExtension(iconFile.FileName);
                var fpath = Path.GetExtension(iconFile.FileName);

                var fileName = fileName_t + DateTime.Now.ToString("yymmssfff") + fpath;
                tblProducts.productPhoto = "/icons/" + fileName;
                fileName = Path.Combine(Server.MapPath("~/icons/"), fileName);
                iconFile.SaveAs(fileName);
            }
            var addedOn = DateTime.Now;
            var isUserID = User.Identity.GetUserId();
            var CID = dba.Users.Where(i => i.Id == isUserID).Select(cid => cid.companyID).SingleOrDefault();
            var addedBy = dba.Users.Where(i => i.Id == isUserID).Select(cid => cid.displayName).SingleOrDefault();
            
            
            if (ModelState.IsValid)
            {
              
                tblProducts.addedOn = addedOn;
                tblProducts.addedBy = addedBy;
                tblProducts.companyID = CID;
                db.tblProducts.Add(tblProducts);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblProducts);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblProducts tblProducts = db.tblProducts.Find(id);
            if (tblProducts == null)
            {
                return HttpNotFound();
            }
            return View(tblProducts);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,productName,productCode,productType,productCategory,productPrice,productAmount,productDesc,addedOn,addedBy,productPhoto,companyID,isActive")] tblProducts tblProducts)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblProducts).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblProducts);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblProducts tblProducts = db.tblProducts.Find(id);
            if (tblProducts == null)
            {
                return HttpNotFound();
            }
            return View(tblProducts);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblProducts tblProducts = db.tblProducts.Find(id);
            db.tblProducts.Remove(tblProducts);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public List<tblProductType> GetProductTypes()
        {
            InventoryContext db = new InventoryContext();
            ApplicationDbContext dba = new ApplicationDbContext();

            var isUserID = User.Identity.GetUserId();
            var CID = dba.Users.Where(i => i.Id == isUserID).Select(cid => cid.companyID).SingleOrDefault();
            //product group listing
            List<tblProductType> Groups = db.productTypes.Where(g => g.companyID == CID).ToList();

            return Groups;
        }
        public List<tblCategories> GetCategories()
        {
            InventoryContext db = new InventoryContext();
            ApplicationDbContext dba = new ApplicationDbContext();

            var isUserID = User.Identity.GetUserId();
            var CID = dba.Users.Where(i => i.Id == isUserID).Select(cid => cid.companyID).SingleOrDefault();
            //product group listing
            List<tblCategories> categories = db.tblCategories.Where(c => c.companyID == CID).ToList();

            return categories;
        }
    }
}
