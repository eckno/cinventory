namespace CInventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.productModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        productCode = c.Int(nullable: false),
                        productName = c.String(),
                        Qty = c.Int(nullable: false),
                        productPrice = c.Int(nullable: false),
                        productDesc = c.String(),
                        dateAdded = c.DateTime(nullable: false),
                        createdBy = c.String(),
                        companyID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.tblProductTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        typeName = c.String(),
                        createdOn = c.DateTime(nullable: false),
                        createdBy = c.String(),
                        companyID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.tblCategories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        categoryName = c.String(),
                        createdOn = c.DateTime(nullable: false),
                        createdBy = c.String(),
                        companyID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.tblCompanies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        companyName = c.String(nullable: false),
                        companyAddress = c.String(nullable: false),
                        companyContactNo = c.String(nullable: false),
                        companySlogan = c.String(),
                        companyEmail = c.String(nullable: false),
                        companyWebsite = c.String(),
                        companyAdminEmail = c.String(nullable: false),
                        companyAdminAuth = c.String(nullable: false),
                        companyID = c.String(),
                        createdOn = c.DateTime(nullable: false),
                        subscription = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.tblCustomers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        firstName = c.String(),
                        lastName = c.String(),
                        companyName = c.String(),
                        address = c.String(),
                        email = c.String(),
                        contactNumber = c.String(),
                        desc = c.String(),
                        addedBy = c.String(),
                        createdOn = c.DateTime(nullable: false),
                        companyID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.tblInvoices",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        productID = c.Int(nullable: false),
                        Qty = c.Int(nullable: false),
                        unitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        totalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        customerID = c.Int(nullable: false),
                        invoiceNo = c.String(),
                        paymentStatus = c.String(),
                        invoiceStatus = c.String(),
                        createdOn = c.DateTime(nullable: false),
                        companyID = c.String(),
                        isActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.tblOrders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        productID = c.Int(nullable: false),
                        Qty = c.Int(nullable: false),
                        unitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        totalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        customerID = c.Int(nullable: false),
                        orderNo = c.String(),
                        orderStatus = c.String(),
                        createdOn = c.DateTime(nullable: false),
                        companyID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.tblProducts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        productName = c.String(nullable: false),
                        productCode = c.String(),
                        productType = c.String(nullable: false),
                        productCategory = c.String(nullable: false),
                        productPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        totalPriceSupplied = c.Decimal(nullable: false, precision: 18, scale: 2),
                        unitSellingPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        vendor = c.String(nullable: false),
                        productAmount = c.Int(nullable: false),
                        suppliedQty = c.Int(nullable: false),
                        productDesc = c.String(),
                        addedOn = c.DateTime(nullable: false),
                        addedBy = c.String(),
                        productPhoto = c.String(),
                        companyID = c.String(),
                        isActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.tblReceipts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        productID = c.Int(nullable: false),
                        Qty = c.Int(nullable: false),
                        unitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        totalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        customerID = c.Int(nullable: false),
                        receiptNo = c.String(),
                        createdOn = c.DateTime(nullable: false),
                        companyID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.tblReceiveds",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        productID = c.Int(nullable: false),
                        receivedBy = c.String(),
                        receivedOn = c.DateTime(nullable: false),
                        receivedAmount = c.Int(nullable: false),
                        vendorID = c.Int(nullable: false),
                        receivedPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        monthReceived = c.String(),
                        companyID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.tblRegistries",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        user = c.String(),
                        desc = c.String(),
                        createdOn = c.String(),
                        companyID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.tblSolds",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        productID = c.Int(nullable: false),
                        Qty = c.Int(nullable: false),
                        customerID = c.Int(nullable: false),
                        unitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        totalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        discount = c.Int(nullable: false),
                        vat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        soldBy = c.String(),
                        soldOn = c.DateTime(nullable: false),
                        companyID = c.String(),
                        invoiceID = c.Int(nullable: false),
                        receiptID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.tblUsers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        firstName = c.String(),
                        middleName = c.String(),
                        surName = c.String(),
                        displayName = c.String(),
                        emailAddress = c.String(),
                        contactNumber = c.String(),
                        contactAddress = c.String(),
                        gender = c.String(),
                        role = c.String(),
                        registeredOn = c.DateTime(nullable: false),
                        profilePics = c.String(),
                        companyID = c.String(),
                        license = c.String(),
                        isActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.tblVendors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        firstName = c.String(),
                        lastName = c.String(),
                        companyName = c.String(),
                        address = c.String(),
                        email = c.String(),
                        contactNumber = c.String(),
                        desc = c.String(),
                        addedBy = c.String(),
                        createdOn = c.DateTime(nullable: false),
                        companyID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.tblWaybills",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        productID = c.Int(nullable: false),
                        Qty = c.Int(nullable: false),
                        unitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        totalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        deliveryFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        country = c.String(),
                        state = c.String(),
                        city = c.String(),
                        customerAddress = c.String(),
                        landMark = c.String(),
                        toReceiveBy = c.String(),
                        customerID = c.Int(nullable: false),
                        invoiceNo = c.String(),
                        receiptNo = c.String(),
                        createdOn = c.DateTime(nullable: false),
                        companyID = c.String(),
                        deliveryStatus = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        firstName = c.String(),
                        lastName = c.String(),
                        displayName = c.String(),
                        companyID = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.tblWaybills");
            DropTable("dbo.tblVendors");
            DropTable("dbo.tblUsers");
            DropTable("dbo.tblSolds");
            DropTable("dbo.tblRegistries");
            DropTable("dbo.tblReceiveds");
            DropTable("dbo.tblReceipts");
            DropTable("dbo.tblProducts");
            DropTable("dbo.tblOrders");
            DropTable("dbo.tblInvoices");
            DropTable("dbo.tblCustomers");
            DropTable("dbo.tblCompanies");
            DropTable("dbo.tblCategories");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.tblProductTypes");
            DropTable("dbo.productModels");
        }
    }
}
