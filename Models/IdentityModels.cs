﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CInventory.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string displayName { get; set; }
        public string companyID { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
    //Inheriting Context
    //public class InventoryDBInitializer : CreateDatabaseIfNotExists<InventoryContext>
    //{
    //    protected override void Seed(InventoryContext context)
    //    {
    //        base.Seed(context);
    //    }
    //}
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("InventoryContext", throwIfV1Schema: false)
        {
            //Database.SetInitializer<InventoryContext>(new CreateDatabaseIfNotExists<InventoryContext>());
            //Database.SetInitializer<InventoryContext>(new DropCreateDatabaseIfModelChanges<InventoryContext>());
            //Database.SetInitializer<InventoryContext>(new DropCreateDatabaseAlways<InventoryContext>());
            //Database.SetInitializer<InventoryContext>(new InventoryContext());
            //Database.SetInitializer<IdentityDbContext>(null);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<productModel> products { get; set; }

        public DbSet<tblCompanies> tblCompanies { get; set; }
        public DbSet<tblCategories> tblCategories { get; set; }
        public DbSet<tblCustomers> tblCustomers { get; set; }
        public DbSet<tblInvoices> tblInvoices { get; set; }
        public DbSet<tblOrders> tblOrders { get; set; }
        public DbSet<tblProducts> tblProducts { get; set; }
        public DbSet<tblProductType> productTypes { get; set; }
        public DbSet<tblReceipts> tblReceipts { get; set; }
        public DbSet<tblReceived> tblReceives { get; set; }
        public DbSet<tblRegistry> tblRegistries { get; set; }
        public DbSet<tblSold> tblSolds { get; set; }
        public DbSet<tblUsers> tblUsers { get; set; }
        public DbSet<tblVendor> tblVendors { get; set; }
        public DbSet<tblWaybill> tblWaybills { get; set; }
    }
}