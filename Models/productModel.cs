﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CInventory.Models
{
    public class productModel
    {
        public int ID { get; set; }
        public int productCode { get; set; }
        public string productName { get; set; }
        public int Qty { get; set; }
        public int productPrice { get; set; }
        public string productDesc { get; set; }
        public DateTime dateAdded { get; set; }
        public string createdBy { get; set; }
        public string companyID { get; set; }
    }
}