﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CInventory.Models
{
    public class tblCategories
    {
        public int ID { get; set; }
        //[Required (ErrorMessage ="Kindly type-in category name!")]
        public string categoryName { get; set; }
        public DateTime createdOn { get; set; }
        public string createdBy { get; set; }
        public string companyID { get; set; }
    }
}