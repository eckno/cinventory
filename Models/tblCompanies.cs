﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CInventory.Models
{
    public class tblCompanies
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "* Kindly fill out Company's Name field")]
        public string companyName { get; set; }
        [Required(ErrorMessage = "* Kindly fill out Company's Address field")]
        public string companyAddress { get; set; }
        [Required(ErrorMessage = "* Kindly fill out Company's Contact Number")]
        public string companyContactNo { get; set; }
        public string companySlogan { get; set; }
        [Required(ErrorMessage = "* Kindly fill out Company's Email Address")]
        public string companyEmail { get; set; }
        public string companyWebsite { get; set; }
        [Required(ErrorMessage = "* Admin Email must be filled")]
        public string companyAdminEmail { get; set; }
        [Required(ErrorMessage = "* Kindly fill out Admin Password")]
        public string companyAdminAuth { get; set; }
        public string companyID { get; set; }
        public DateTime createdOn { get; set; }
        [Required(ErrorMessage = "* Kindly select a subscription package.")]
        public string subscription { get; set; }
    }
}