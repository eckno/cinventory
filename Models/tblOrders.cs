﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CInventory.Models
{
    public class tblOrders
    {
        public int ID { get; set; }
        public int productID { get; set; }
        public int Qty { get; set; }
        public Decimal unitPrice { get; set; }
        public Decimal totalPrice { get; set; }
        public int customerID { get; set; }
        public string orderNo { get; set; }
        public string orderStatus { get; set; }
        public DateTime createdOn { get; set; }
        public string companyID { get; set; }
    }
}