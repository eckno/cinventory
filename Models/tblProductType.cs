﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CInventory.Models
{
    public class tblProductType
    {
        public int ID { get; set; }
        public string typeName { get; set; }
        public DateTime createdOn { get; set; }
        public string createdBy { get; set; }
        public string companyID { get; set; }
    }
}