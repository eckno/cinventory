﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CInventory.Models
{
    public class tblProducts
    {
        public int ID { get; set; }
        [Required (ErrorMessage ="* Product Name is required.")]
        public string productName { get; set; }
        public string productCode { get; set; }
        [Required(ErrorMessage = "* Product Type is required.")]
        public string productType { get; set; }
        [Required(ErrorMessage = "* Product Category is required.")]
        public string productCategory { get; set; }
        [Required(ErrorMessage = "* Kindly enter purchase price per unit of this item!")]
        public Decimal productPrice { get; set; }
        [Required(ErrorMessage = "* Kindly enter total price of supplied item!")]
        public Decimal totalPriceSupplied { get; set; }
        [Required(ErrorMessage = "* Kindly enter selling price per unit of this item!")]
        public Decimal unitSellingPrice { get; set; }
        [Required(ErrorMessage = "* Kindly enter supplied Quantity!")]
        public string vendor { get; set; }
        public int productAmount { get; set; }
        public int suppliedQty { get; set; }
        public string productDesc { get; set; }
        public DateTime addedOn { get; set; }
        public string addedBy { get; set; }
        public string productPhoto { get;  set; }
        public string companyID { get; set; }
        public bool isActive { get; set; }
        //[NotMapped]
        //public HttpPostedFileBase iconFile { get; set; }
    }
}