﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CInventory.Models
{
    public class tblReceived
    {
        public int ID { get; set; }
        public int productID { get; set; }
        public string receivedBy { get; set; }
        public DateTime receivedOn { get; set; }
        public int receivedAmount { get; set; }
        public int vendorID { get; set; }
        public Decimal receivedPrice { get; set; }
        public string monthReceived { get; set; }
        public string companyID { get; set; }
    }
}