﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CInventory.Models
{
    public class tblRegistry
    {
        public int ID { get; set; }
        public string user { get; set; }
        public string desc { get; set; }
        public string createdOn { get; set; }
        public string companyID { get; set; }
    }
}