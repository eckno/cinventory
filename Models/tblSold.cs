﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CInventory.Models
{
    public class tblSold
    {
        public int ID { get; set; }
        public int productID { get; set; }
        public int Qty { get; set; }
        public int customerID { get; set; }
        public Decimal unitPrice { get; set; }
        public Decimal totalPrice { get; set; }
        public int discount { get; set; }
        public Decimal vat { get; set; }
        public string soldBy { get; set; }
        public DateTime soldOn { get; set; }
        public string companyID { get; set; }
        public int invoiceID { get; set; }
        public int receiptID { get; set; }
    }
}