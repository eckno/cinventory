﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CInventory.Models
{
    public class tblUsers
    {
        public int ID { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string surName { get; set; }
        public string displayName { get; set; }
        public string emailAddress { get; set; }
        public string contactNumber { get; set; }
        public string contactAddress { get; set; }
        public string gender { get; set; }
        public string role { get; set; }
        public DateTime registeredOn { get; set; }
        public string profilePics { get; set; }
        public string companyID { get; set; }
        public string license { get; set; }
        public bool isActive { get; set; }
    }
}