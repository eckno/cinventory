﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CInventory.Models
{
    public class tblVendor
    {
        public int ID { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string companyName { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public string contactNumber { get; set; }
        public string desc { get; set; }
        public string addedBy { get; set; }
        public DateTime createdOn { get; set; }
        public string companyID { get; set; }
    }
}