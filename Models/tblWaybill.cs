﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CInventory.Models
{
    public class tblWaybill
    {
        public int ID { get; set; }
        public int productID { get; set; }
        public int Qty { get; set; }
        public Decimal unitPrice { get; set; }
        public Decimal totalPrice { get; set; }
        public Decimal deliveryFee { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string customerAddress { get; set; }
        public string landMark { get; set; }
        public string toReceiveBy { get; set; }
        public int customerID { get; set; }
        public string invoiceNo { get; set; }
        public string receiptNo { get; set; }
        public DateTime createdOn { get; set; }
        public string companyID { get; set; }
        public string deliveryStatus { get; set; }
    }
}