﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CInventory.Startup))]
namespace CInventory
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
